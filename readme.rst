#####
Extension sphinx - sélection de code
#####

Ce dépôt contient le code-source d'une extension sphinx codée en ``javascript`` qui permet de sélectionner le contenu d'un ``code-block``.

Pour ajouter cette extension à un projet sphinx existant, veuillez suivre les étapes suivantes.

1. Créez un dossier ``_static`` dans le répertoire ``./sources``
2. Ajoutez-y le fichier ``code_selection.js`` de ce dépôt
3. Ajoutez le code suivant au fichier ``./source/conf.py`` :

.. code-block:: python

	def setup(app): app.add_javascript('code_selection.js')

4. Recréez les fichiers HTML pour qu'ils contiennent la nouvelle extension :

.. code-block:: sh

	rm -r build/html
	make html